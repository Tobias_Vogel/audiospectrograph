#!/usr/bin/env python
# coding: utf-8 

#   This software component is licensed by ST under BSD 3-Clause license,
#   the "License"; You may not use this file except in compliance with the
#   License. You may obtain a copy of the License at:
#                        https://opensource.org/licenses/BSD-3-Clause
  

"""ASC Feature Extraction example."""

import matplotlib.pyplot as plt
import numpy as np
import librosa
import librosa.display
import scipy.fftpack as fft
import wave
from sklearn.metrics import confusion_matrix
from scipy.io import wavfile
from tensorflow import keras
from tensorflow.keras.utils import to_categorical
import joblib

audioChannels = 1 #mono= True
samplingRate = 16000 #samples/sec
fftSamples = 1024
melBands = 30
spectrogramCols = 32
spectrogramsPerFile = 28 # a spectrogram every 1024ms
spectrogramsPerSequence = 1  # for post processing time filtering
num_epochs = 20

#   Paths
dataSetPath = './Dataset/'
featureSetsPath = './Dataset/FeatureSet/'
logPath = './Output/'
waveSetPath = './Wave/16000/'

sessionNum = 'FixFrequencyCheck'

model = keras.models.load_model('./Output/Session_FixFrequencyTrain_Model.h5')

# 3 classes : 0 high_frequency, 1 middle_frequency, 2 low_frequency
ascLabels = {
            'high_frequency' : 0,
            'middle_frequency' : 1,
            'low_frequency' : 2,
}

nclasses = len(ascLabels)
#Label Font Size for plots
labFontSize = 'xx-small' if (nclasses == 15) else 'medium'

### 3 classes : 0 high_frequency, 1 middle_frequency, 2 low_frequency
ascLabels_3cl = {
            'low' : 2,
            'fast' : 0,
            'slow' : 2,
            'middle' : 1,
            'high' : 0,
            'ultra' : 0,
            'room' : 1,
            'music' : 1,
            'upper' : 0,
            'infra' : 2,
            'deep' :  2,
            'below' :  2,
}

def wave_load(filename):
    '''load a .wav file from the dataset and returns float normalized samples'''
    
    audio_file = wave.open(filename, 'rb')
    sample_rate = audio_file.getframerate()
    sample_width = audio_file.getsampwidth()
    number_of_channels = audio_file.getnchannels()
    number_of_frames = audio_file.getnframes()

    data = audio_file.readframes(number_of_frames)
    audio_file.close()

    y_wav_int = np.frombuffer(data, 'int16') # deprecated
    y_wav_float_librosa = librosa.util.buf_to_float(y_wav_int, n_bytes = 2, dtype = np.float32)
    
    return y_wav_float_librosa

def create_col(y):
    '''calculates a single column of Spectrogram from 1024 audio samples'''
    assert y.shape == (fftSamples,)

    # Create time-series window
    fft_window = librosa.filters.get_window('hann', fftSamples, fftbins = True)
    # fft_window = fft_window.astype(np.float32)
    assert fft_window.shape == (fftSamples,), fft_window.shape

    # Hann window
    y_windowed = fft_window * y
    assert y_windowed.shape == (fftSamples,), y_windowed.shape

    # FFT
    fft_out = fft.fft(y_windowed, axis = 0)[:513]
    assert fft_out.shape == (513,), fft_out.shape

    # Power spectrum
    S_pwr = np.abs(fft_out)**2
    assert S_pwr.shape == (513,)

    # Generation of Mel Filter Banks
    mel_basis = librosa.filters.mel(samplingRate, n_fft = fftSamples, n_mels = melBands, htk = False)
    # mel_basis.astype(np.float32)
    assert mel_basis.shape == (melBands, 513)

    # Apply Mel Filter Banks
    S_mel = np.dot(mel_basis, S_pwr)
    # S_mel.astype(np.float32)
    assert S_mel.shape == (melBands,)

    return S_mel

def spectrogram_normalization_dB(Spectrogram):
    # Scale according to reference power?
    Spectrogram = Spectrogram / Spectrogram.max()
    # Convert to dB
    S_log_mel = librosa.power_to_db(Spectrogram, top_db = 80.0)
    return S_log_mel

#Loading Annotated Text filelists.
testFileSet  = np.loadtxt(dataSetPath + 'TestSet.txt',dtype='str')

#####         Data Set Loading and Feature Scaler Application           #######

# Load Scaler and apply to test Sets
scaler  = joblib.load(featureSetsPath + 'zscore_scaler.pkl')

########                             Test Set                    #######
# Allocation of Test Set vectors
X_test = np.empty([len(testFileSet)*spectrogramsPerFile,melBands,spectrogramCols], dtype='float32', order='C')
Y_test = np.empty([len(testFileSet)*spectrogramsPerFile],dtype='int32')

##Building Test Set X and Y  !!!
print ('Building Fetures for Test Set....\n\n')
for i in range(testFileSet.shape[0]):
    print ('Opening audio file %s:\t %d of %d \n' % (testFileSet[i,0],i+1,testFileSet.shape[0]))
    sig = wave_load(waveSetPath + testFileSet[i,0])

    # For debug purpose (usable in the STM32 world)
    # =============================================
    """"
    file = open("InputWavDataDemoPython500Hz.txt", "w")
    for x in range(480000):
          file.write("%s,\n" %(sig[x]))
    file.close()
    # =============================================
    """

    # Sequence segmentation
    frames = librosa.util.frame(sig, frame_length = fftSamples, hop_length = 512) # 1024, 936 samples frame
    logMelsSequence = np.empty([melBands, frames.shape[1]], dtype = 'float32', order = 'C')
    
    #extracting spectrograms
    for j in range(frames.shape[1]):
        logMelsSequence[:, j] = create_col(frames[:, j])
    
    for k in range(spectrogramsPerFile):
        # Spectrogram extraction from current sequence
        S_mel = logMelsSequence[:, k * spectrogramCols:(k+1) * spectrogramCols]
        # Spectrogram Normalization and dB
        S_log_mel = spectrogram_normalization_dB(S_mel)

        # Add to the global Test Set vectors
        X_test[i * spectrogramsPerFile+k] = S_log_mel
        Y_test[i * spectrogramsPerFile+k] = ascLabels_3cl[testFileSet[i, 1]]

#########        Scaler application to Test Sets        
X_test_r = X_test.reshape(X_test.shape[0], melBands * spectrogramCols)
X_test_r_scaled = scaler.transform(X_test_r)
X_test = X_test_r_scaled.reshape(X_test.shape[0], melBands, spectrogramCols, 1)

#delete redundant vectors
del(X_test_r)
del(X_test_r_scaled)

#Labels Categorization
Y_test_cat = to_categorical(Y_test, nclasses)

##############            Test set Evaluation               ###################
score = model.evaluate(X_test, Y_test_cat, verbose = 2)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

# For debug purpose (usable in the STM32 world)
# =============================================
""""
file = open("InputNN_DataDemoPython500_1000_2000Hz.txt", "w")
fNN_Val = X_test.reshape(80640, 1, 1, 1)
for x in range(80640):
    file.write("%s,\n" %(fNN_Val[x][0][0][0]))
file.close()
# =============================================
"""

#%%
#####        Test Set Prediction - confusion Matrix on sequence base       ###

test_set_pred = model.predict(X_test)
print('Saving Model Prediction Outputs for Test Set: ......\n\n')
joblib.dump(test_set_pred, logPath + 'Session_' + sessionNum + '_Test_Set_predictions' + '.pkl', compress = 5)

#Time filtering for the test Set on a sequence base (30")
Y_test_f = Y_test[::spectrogramsPerSequence]
test_set_pred_f = np.array([],dtype = 'int64')
#Time filtering for the test Set
for i in range(0,test_set_pred.shape[0],spectrogramsPerSequence):
    sequence_pred = test_set_pred[i:i + spectrogramsPerSequence].mean(axis = 0).argmax()
    test_set_pred_f = np.append(test_set_pred_f, sequence_pred)

# Build and save confusion matrix on Test Set
test_conf_mtx = confusion_matrix(Y_test_f, test_set_pred_f)
np.savetxt(logPath + 'Session_' + sessionNum + '_Test_Set_confMatrix' + '.csv',test_conf_mtx, fmt = '%7d', delimiter = ',', header = 'Session_' + sessionNum + '_test Confusion Matrix')

# Calculate confusion matrix percentages
test_confMtx_perc = test_conf_mtx.astype('float') / test_conf_mtx.sum(axis = 1)[:, np.newaxis] * 100.0

# Confusion Matrix Plotting and Saving
fig = plt.figure()
plt.clf()
ax = fig.add_subplot(111)
ax.set_aspect(1)
ax.get_yaxis().set_tick_params(direction = 'out')
ax.get_xaxis().set_tick_params(direction = 'out')
res = ax.imshow(np.array(test_confMtx_perc), cmap = plt.cm.YlGnBu_r, interpolation = 'nearest')

for x in range(nclasses):
    for y in range(nclasses):
        annotation = str((test_confMtx_perc[x][y])).split('.')[0] + '.' + str((test_confMtx_perc[x][y])).split('.')[1][0]
        ax.annotate(annotation, xy = (y, x), fontsize = labFontSize, color = '#7f7f7f', horizontalalignment = 'center', verticalalignment = 'center')

plt.title('Test Set Confusion Matrix %')
plt.xticks(range(nclasses), sorted(ascLabels, key = ascLabels.get), rotation = 90)
plt.yticks(range(nclasses), sorted(ascLabels, key = ascLabels.get))
plt.tight_layout()
plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.tight_layout()
plt.savefig(logPath + 'Session_' + sessionNum + '_Set_confMatrix' + '.png', dpi = 200, format = 'png')

print('Analyzing result of ' + testFileSet[0, 0] + ': ' + list(ascLabels)[test_set_pred_f[0]] + '\n')
print('Analyzing result of ' + testFileSet[1, 0] + ': ' + list(ascLabels)[test_set_pred_f[28]] + '\n')
print('Analyzing result of ' + testFileSet[2, 0] + ': ' + list(ascLabels)[test_set_pred_f[56]] + '\n')