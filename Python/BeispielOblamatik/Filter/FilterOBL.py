import sounddevice as sd
import matplotlib.pyplot as plt
from scipy.io import wavfile
import librosa
import librosa.display
import sklearn
import numpy as np
import argparse
from sklearn.metrics import confusion_matrix
from tensorflow import keras
import os
from scipy import signal
from scipy.io.wavfile import read
from scipy.io.wavfile import write     # Imported libaries such as numpy, scipy(read, write), matplotlib.pyplot


plt.title('Original Signal Spectrum')
plt.xlabel('Frequency(Hz)')
plt.ylabel('Amplitude')

arr = os.listdir('./wav_44100_OBL/')

b,a = signal.butter(5, 0.02, btype='highpass') # ButterWorth filter 4350

for file in arr:
    print(file)
    plt.clf()
    
    (Frequency, indata) = read('./wav_44100_OBL/' + file) # Reading the sound file. 
    data = indata
    amp = 1.0
    recData = amp * data / max(abs(max(data)),abs(min(data)))
    plt.plot(recData) # plotting the signal.
    plt.plot(indata) # plotting the signal.
    filteredSignal = signal.lfilter(b,a,recData)
    plt.plot(filteredSignal) # plotting the signal.
    write('./wav_16000_OBL/' + file, 16000, filteredSignal)