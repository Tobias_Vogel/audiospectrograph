/**
  ******************************************************************************
  * @file    audio_process.c
  * @author  Tobias Vogel @ OBLAMATIK
  * @brief   This module implements the audio process function.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 Oblamatik AG.
  * All rights reserved.</center></h2>
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "menu.h"
#include "app_x-cube-ai.h"
#include "asc_processing.h"
#include "audio_process.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/* Audio frequency */
#define AUDIO_FREQUENCY            AUDIO_FREQUENCY_16K
#define AUDIO_IN_PDM_BUFFER_SIZE   ((uint32_t)(128 * AUDIO_FREQUENCY / 16000 * 2))
#define AUDIO_NB_BLOCKS    ((uint32_t)4)
#define AUDIO_BLOCK_SIZE   ((uint32_t)0xFFFE)
/* Size of the recorder buffer */
#define RECORD_BUFFER_SIZE        4096
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Define record Buf at D3SRAM @0x38000000 since the BDMA for SAI4 use only this memory */
ALIGN_32BYTES (uint16_t recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE]) __attribute__((section(".RAM_D3")));
ALIGN_32BYTES (uint16_t  RecPlayback[2*RECORD_BUFFER_SIZE]);
uint32_t VolumeLevelOutput = 60;
uint32_t VolumeLevelInput = 80;
uint32_t  InState = 0;
uint32_t  OutState = 0;
BSP_AUDIO_Init_t  AudioInInit;
BSP_AUDIO_Init_t  AudioOutInit;
/* Pointer to record_data */
uint32_t playbackPtr = 0;
uint32_t AudioBufferOffset;
uint8_t ui8ProcessAudioRingbuffer = 0;
uint8_t uc8BlinkLED = 0;
float32_t Proc_Buffer_f[FILL_BUFFER_SIZE];
int16_t Fill_Buffer[FILL_BUFFER_SIZE];
static uint32_t index_buff_fill = 0;

static volatile int           ButtonPressed    = 0;

#ifndef USE_STM32L475E_IOT01
static volatile int           MEMSInterrupt    = 0;
#endif /* USE_STM32L475E_IOT01 */

volatile int RebootBoard                       = 0;

static volatile uint32_t      RunASCEvent      = 0;
static volatile uint32_t      SendEnv          = 0;
static volatile uint32_t      SendAudioLevel   = 0;
static volatile uint32_t      SendAccGyroMag   = 0;
static volatile uint32_t      ledTimer         = 0;
static volatile uint32_t UpdateMotionAR        = 0;

/* Imported Variables --------------------------------------------------------*/
extern uint16_t PCM_Buffer[];
extern uint8_t set_connectable;
extern volatile float RMS_Ch[];
extern float DBNOISE_Value_Old_Ch[];

/* Exported Variables --------------------------------------------------------*/
uint8_t BufferToWrite[256];
int32_t BytesToWrite;
ASC_OutputTypeDef ascResultStored = ASC_UNDEFINED;

/* Private function prototypes -----------------------------------------------*/
static void AudioProcess(void);
static void RunASC(void);
static void AudioProcess_FromMic(uint16_t *pData, uint32_t count);

typedef enum {
  BUFFER_OFFSET_NONE = 0,
  BUFFER_OFFSET_HALF,
  BUFFER_OFFSET_FULL,
}BUFFER_StateTypeDef;
/* Private functions ---------------------------------------------------------*/

/**
  * @brief Test Audio record.
  *   The main objective of this test is to check the hardware connection of the
  *   Audio peripheral.
  * @param  None
  * @retval None
*/
void AudioRecord_demo(void)
{
  uint32_t channel_nbr = 2;

  uint32_t x_size, y_size;

  BSP_LCD_GetXSize(0, &x_size);
  BSP_LCD_GetYSize(0, &y_size);

  /* Clear the LCD */
  UTIL_LCD_Clear(UTIL_LCD_COLOR_WHITE);
  /* Set Audio Demo description */
  UTIL_LCD_FillRect(0, 0, x_size, 90, UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_SetTextColor(UTIL_LCD_COLOR_WHITE);
  UTIL_LCD_SetBackColor(UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_SetFont(&Font24);
  UTIL_LCD_DisplayStringAt(0, 0, (uint8_t *)"DETECTION OF THE BUSINESS SIZE (BIG or small)", CENTER_MODE);
  UTIL_LCD_SetFont(&Font16);
  UTIL_LCD_DisplayStringAt(0, 24, (uint8_t *)"Make sure the microphone is attached to the toilet!", CENTER_MODE);
  UTIL_LCD_DisplayStringAt(0, 40,  (uint8_t *)"Press blue button for stop detection.", CENTER_MODE);
  /* Set the LCD Text Color */
  UTIL_LCD_DrawRect(10, 100, x_size - 20, y_size - 110, UTIL_LCD_COLOR_BLUE);
  UTIL_LCD_DrawRect(11, 101, x_size - 22, y_size - 112, UTIL_LCD_COLOR_BLUE);

  AudioOutInit.Device = AUDIO_OUT_DEVICE_HEADPHONE;
  AudioOutInit.ChannelsNbr = channel_nbr;
  AudioOutInit.SampleRate = AUDIO_FREQUENCY;
  AudioOutInit.BitsPerSample = AUDIO_RESOLUTION_16B;
  AudioOutInit.Volume = VolumeLevelOutput;

  AudioInInit.Device = AUDIO_IN_DEVICE_DIGITAL_MIC;
  AudioInInit.ChannelsNbr = channel_nbr;
  AudioInInit.SampleRate = AUDIO_FREQUENCY;
  AudioInInit.BitsPerSample = AUDIO_RESOLUTION_16B;
  AudioInInit.Volume = VolumeLevelInput;

  BSP_JOY_Init(JOY1, JOY_MODE_GPIO, JOY_ALL);

  /* Initialize Audio Recorder with 2 channels to be used */
  BSP_AUDIO_IN_Init(1, &AudioInInit);
  BSP_AUDIO_IN_GetState(1, &InState);

  BSP_AUDIO_OUT_Init(0, &AudioOutInit);

  /* Start Recording */
  BSP_AUDIO_IN_RecordPDM(1, (uint8_t*)&recordPDMBuf, 2*AUDIO_IN_PDM_BUFFER_SIZE);

  /* Play the recorded buffer */
  BSP_AUDIO_OUT_Play(0, (uint8_t*)&RecPlayback[0], 2*RECORD_BUFFER_SIZE);

  while (1)
  {
    if (CheckForUserInput() > 0)
    {
      ASC_DeInit();

      ButtonState = 0;
      BSP_AUDIO_OUT_Stop(0);
      BSP_AUDIO_OUT_DeInit(0);
      BSP_AUDIO_IN_Stop(1);
      BSP_AUDIO_IN_DeInit(1);
      return;
    }

    /* ASC */
    if (RunASCEvent)
    {
    	RunASCEvent = 0;
    	RunASC();
    }
  }
}

/**
* @brief  Callback function when 1ms PCM Audio is received from Microphone
* @param  none
* @retval None
*/
static void AudioProcess_FromMic(uint16_t *pData, uint32_t count)
 {
	static uint8_t ui8LedBlueState = 0;

	/* Copy PCM Buffer on FILL BUFFER */
	/* In this Audio configuration, the signal comes in as 2ch, extract 1ch */
	for (uint32_t i = 0; i < count / 2; i++)
	{
		Fill_Buffer[index_buff_fill] = pData[2 * i];
		index_buff_fill++;
	}

	if (ui8LedBlueState)
	{
		BSP_LED_On(LED_BLUE); // blue LED on
		ui8LedBlueState = 0;
	}
	else
	{
		BSP_LED_Off(LED_BLUE); // blue LED off
		ui8LedBlueState = 1;
	}

	AudioProcess();
}

/**
 * @brief  Function that is called when data has been appended to Fill Buffer
 * @param  none
 * @retval None
 */
static void AudioProcess(void)
{
	static uint8_t ui8LedRedState = 0;
	float32_t sample;

	/* Create a 64 ms (1024 samples) window every 32 ms (512 samples)
	   Audio Feature Extraction is ran every 32 ms on a 64 ms window (50% overlap) */
	if (index_buff_fill == FILL_BUFFER_SIZE)
	{
		/* Copy Fill Buffer in Proc Buffer */
		for (uint32_t i = 0; i < FILL_BUFFER_SIZE; i++)
		{
			sample = ((float32_t) Fill_Buffer[i]);
			/* Invert the scale of the data */
			sample /= (float32_t) ((1 << (8 * sizeof(int16_t) - 1)));
			Proc_Buffer_f[i] = sample;
		}

		/* Left shift Fill Buffer by 512 samples */
		memmove(Fill_Buffer, Fill_Buffer + FILL_BUFFER_LEFT_SHIFT_SIZE, sizeof(int16_t) * FILL_BUFFER_LEFT_SHIFT_SIZE);
		index_buff_fill = FILL_BUFFER_LEFT_SHIFT_SIZE;

		/* Release processing thread to start Audio Feature Extraction */
		RunASCEvent = 1;

		if (ui8LedRedState)
		{
			BSP_LED_On(LED3); // red LED on
			ui8LedRedState = 0;
		}
		else
		{
			BSP_LED_Off(LED3); // red LED off
			ui8LedRedState = 1;
		}
	}
}

/**
  * @brief  Acoustic Scene Recognition Working function
  * @param  None
  * @retval None
  */
static void RunASC(void)
{
  ASC_OutputTypeDef classification_result;
  static uint8_t ui8LedGreenState = 0;
  static uint8_t ui8ASC_RunCnt = 0;

  /* ASC_Run needs to be called 32 times before it can run the NN and return a classification */
  float32_t afFFTBase[1024];
  for (uint32_t u32Indx = 0; u32Indx < 1024; u32Indx++)
  {
	  afFFTBase[u32Indx] = Proc_Buffer_f[u32Indx];
  }
  ASC_Run(&afFFTBase[0]);
  //TBDclassification_result = ASC_Run(Proc_Buffer_f);

  /* Only display classification result if a valid classification is returned */
  if (classification_result != ASC_UNDEFINED)
  {
	  if (ascResultStored != classification_result)
	  {
		  ascResultStored = classification_result;

		  BytesToWrite = sprintf((char *)BufferToWrite,"Sending: ASC=%d", classification_result);
		  UTIL_LCD_DisplayStringAt(0, 120,  (uint8_t *)BufferToWrite, CENTER_MODE);
	  }

	  if (ui8LedGreenState)
	  {
		  BSP_LED_On(LED_GREEN); // green LED on
		  ui8LedGreenState = 0;
	  }
	  else
	  {
		  BSP_LED_Off(LED_GREEN); // green LED off
		  ui8LedGreenState = 1;
	  }
  }
}

/**
* @brief  Half Transfer user callback, called by BSP functions.
* @param  None
* @retval None
*/
void BSP_AUDIO_IN_HalfTransfer_CallBack(uint32_t Instance)
{
	if (Instance == 1U)
	{
		/* Invalidate Data Cache to get the updated content of the SRAM*/
		SCB_InvalidateDCache_by_Addr((uint32_t *)&recordPDMBuf[0], AUDIO_IN_PDM_BUFFER_SIZE*2);

		BSP_AUDIO_IN_PDMToPCM(Instance, (uint16_t*)&recordPDMBuf[0], &RecPlayback[playbackPtr]);

		/* Clean Data Cache to update the content of the SRAM */
		SCB_CleanDCache_by_Addr((uint32_t*)&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4);

		AudioProcess_FromMic(&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4/2);

		playbackPtr += AUDIO_IN_PDM_BUFFER_SIZE/4/2;
		if(playbackPtr >= RECORD_BUFFER_SIZE)
		{
			playbackPtr = 0;
		}
	}
	else
	{
		AudioBufferOffset = BUFFER_OFFSET_HALF;
	}
}

/**
* @brief  Transfer Complete user callback, called by BSP functions.
* @param  None
* @retval None
*/
void BSP_AUDIO_IN_TransferComplete_CallBack(uint32_t Instance)
{
	if (Instance == 1U)
	{
		/* Invalidate Data Cache to get the updated content of the SRAM*/
		SCB_InvalidateDCache_by_Addr((uint32_t *)&recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE/2], AUDIO_IN_PDM_BUFFER_SIZE*2);

		BSP_AUDIO_IN_PDMToPCM(Instance, (uint16_t*)&recordPDMBuf[AUDIO_IN_PDM_BUFFER_SIZE/2], &RecPlayback[playbackPtr]);

		/* Clean Data Cache to update the content of the SRAM */
		SCB_CleanDCache_by_Addr((uint32_t*)&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4);

		AudioProcess_FromMic(&RecPlayback[playbackPtr], AUDIO_IN_PDM_BUFFER_SIZE/4/2);

		playbackPtr += AUDIO_IN_PDM_BUFFER_SIZE/4/2;
		if(playbackPtr >= RECORD_BUFFER_SIZE)
		{
			playbackPtr = 0;
		}

	}
	else
	{
		AudioBufferOffset = BUFFER_OFFSET_FULL;
	}
}

/**
  * @brief  Audio IN Error callback function
  * @param  None
  * @retval None
  */
void BSP_AUDIO_IN_Error_CallBack(uint32_t Instance)
{
  /* Stop the program with an infinite loop */
  Error_Handler();
}
/**
  * @}
  */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
