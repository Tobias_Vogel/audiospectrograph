/**
  ******************************************************************************
  * @file    asc.h
  * @author  MCD Application Team
  * @version V4.0.0
  * @date    30-Oct-2019
  * @brief   Audio Scene Classification APIs
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2018 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _ASC_H_
#define _ASC_H_

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "arm_math.h"

/* Exported define ------------------------------------------------------------*/
#define NFFT                 FILL_BUFFER_SIZE // ORIGINALLY: FILL_BUFFER_SIZE (1024)
#define NMELS                              30 // ORIGINALLY: 30

#define SPECTROGRAM_ROWS                NMELS // ORIGINALLY: NMELS (30)
#define SPECTROGRAM_COLS                   32 // ORIGINALLY: 32
#define MELS_SPECT_SAMPLING_RATE        16000 // 22050 // ORIGINALLY: 16000
#define MELS_SPECT_FFT_LEN               1024 // ORIGINALLY: 1024
#define MELS_SPECT_FRAME_LEN             1024 // ORIGINALLY: 1024

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  ASC_SMALL_BUSINESS = 0x00,
  ASC_BIG_BUSINESS   = 0x01,
  ASC_NOISE          = 0x02,
  ASC_UNDEFINED      = 0xFF
} ASC_OutputTypeDef;

typedef enum
{
  ASC_OK      = 0x00,
  ASC_ERROR   = 0x01,
} ASC_StatusTypeDef;

/* Exported macros -----------------------------------------------------------*/

/** @defgroup NN_ASC_Exported_Functions NN_ASC_Exported_Functions
 * @{
 */

/* Exported constants --------------------------------------------------------*/
#define FILL_BUFFER_SIZE (1024)// * 16)//TBD
#define FILL_BUFFER_LEFT_SHIFT_SIZE (FILL_BUFFER_SIZE / 2)//(FILL_BUFFER_SIZE - 1638)//TBD

ASC_StatusTypeDef ASC_Init(void);

ASC_StatusTypeDef ASC_DeInit(void);

ASC_OutputTypeDef ASC_Run(float32_t *pBuffer);

ASC_StatusTypeDef ASC_NN_Run(float32_t *pSpectrogram, float32_t *pNetworkOut);

ASC_OutputTypeDef ASC_GetClassificationCode(void);

/**
  * @}
  */


#ifdef __cplusplus
}
#endif

#endif /* _ASC_H_ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
