import wave

infiles = [
           "Duenn_Mittel_Loctite_Bubble210919_1055_5.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_6.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_7.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_8.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_9.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_10.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_11.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_12.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_13.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_14.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_15.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_16.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_17.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_18.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_19.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_20.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_21.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_22.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_23.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_24.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_25.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_26.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_27.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_28.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_50.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_51.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_52.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_53.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_54.wav",
           "Duenn_Mittel_Loctite_Bubble210919_1055_55.wav",
          ]
outfile = "Duenn_1.wav"

data= []
for infile in infiles:
    w = wave.open(infile, 'rb')
    data.append( [w.getparams(), w.readframes(w.getnframes())] )
    w.close()
    
output = wave.open(outfile, 'wb')
output.setparams(data[0][0])
for i in range(len(data)):
    output.writeframes(data[i][1])
output.close()