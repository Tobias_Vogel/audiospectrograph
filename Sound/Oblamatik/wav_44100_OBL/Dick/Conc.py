import wave

infiles = [
           "Dick_Loctite_Bubble210919_1025_1202.wav",
           "Dick_Loctite_Bubble210919_1025_1203.wav",
           "Dick_Loctite_Bubble210919_1025_1204.wav",
           "Dick_Loctite_Bubble210919_1025_1205.wav",
           "Dick_Loctite_Bubble210919_1025_1206.wav",
           "Dick_Loctite_Bubble210919_1025_1207.wav",
           "Dick_Loctite_Bubble210919_1025_1208.wav",
           "Dick_Loctite_Bubble210919_1025_1209.wav",
           "Dick_Loctite_Bubble210919_1025_1210.wav",
           "Dick_Loctite_Bubble210919_1025_1214.wav",
           "Dick_Loctite_Bubble210919_1025_1215.wav",
           "Dick_Loctite_Bubble210919_1025_1217.wav",
           "Dick_Loctite_Bubble210919_1025_1218.wav",
           "Dick_Loctite_Bubble210919_1025_1219.wav",
           "Dick_Loctite_Bubble210919_1025_1220.wav",
           "Dick_Loctite_Bubble210919_1025_1221.wav",
           "Dick_Loctite_Bubble210919_1025_1222.wav",
           "Dick_Loctite_Bubble210919_1025_1223.wav",
           "Dick_Loctite_Bubble210919_1025_1224.wav",
           "Dick_Loctite_Bubble210919_1025_1225.wav",
           "Dick_Loctite_Bubble210919_1025_1226.wav",
           "Dick_Loctite_Bubble210919_1025_1227.wav",
           "Dick_Loctite_Bubble210919_1025_1228.wav",
           "Dick_Loctite_Bubble210919_1025_1229.wav",
           "Dick_Loctite_Bubble210919_1025_1230.wav",
           "Dick_Loctite_Bubble210919_1025_1231.wav",
           "Dick_Loctite_Bubble210919_1025_1232.wav",
           "Dick_Loctite_Bubble210919_1025_1233.wav",
           "Dick_Loctite_Bubble210919_1025_1234.wav",
           "Dick_Loctite_Bubble210919_1025_1235.wav"
          ]
outfile = "Dick_1.wav"

data= []
for infile in infiles:
    w = wave.open(infile, 'rb')
    data.append( [w.getparams(), w.readframes(w.getnframes())] )
    w.close()
    
output = wave.open(outfile, 'wb')
output.setparams(data[0][0])
for i in range(len(data)):
    output.writeframes(data[i][1])
output.close()