import wave

infiles = [
           "Dick_Loctite_1_210901_1237_0.wav",
           "Dick_Loctite_1_210901_1237_1.wav",
           "Dick_Loctite_1_210901_1237_4.wav",
           "Dick_Loctite_1_210901_1237_7.wav",
           "Dick_Loctite_1_210901_1237_11.wav",
           "Dick_Loctite_1_210901_1237_12.wav",
           "Dick_Loctite_1_210901_1237_13.wav",
           "Dick_Loctite_1_210901_1237_14.wav",
           "Dick_Loctite_1_210901_1237_16.wav",
           "Dick_Loctite_1_210901_1237_17.wav",
           "Dick_Loctite_1_210901_1237_18.wav",
           "Dick_Loctite_1_210901_1237_19.wav",
           "Dick_Loctite_1_210901_1237_20.wav",
           "Dick_Loctite_1_210901_1237_21.wav",
           "Dick_Loctite_1_210901_1237_22.wav",
           "Dick_Loctite_1_210901_1237_23.wav",
           "Dick_Loctite_1_210901_1237_24.wav",
           "Dick_Loctite_1_210901_1237_25.wav",
           "Dick_Loctite_1_210901_1237_26.wav",
           "Dick_Loctite_1_210901_1237_27.wav",
           "Dick_Loctite_1_210901_1237_28.wav",
           "Dick_Loctite_1_210901_1237_29.wav",
           "Dick_Loctite_1_210901_1237_30.wav",
           "Dick_Loctite_1_210901_1237_31.wav",
           "Dick_Loctite_1_210901_1237_32.wav",
           "Dick_Loctite_1_210901_1237_33.wav",
           "Dick_Loctite_1_210901_1237_34.wav",
           "Dick_Loctite_1_210901_1237_35.wav",
           "Dick_Loctite_1_210901_1237_36.wav",
           "Dick_Loctite_1_210901_1237_37.wav",
          ]
outfile = "Noise_1.wav"

data= []
for infile in infiles:
    w = wave.open(infile, 'rb')
    data.append( [w.getparams(), w.readframes(w.getnframes())] )
    w.close()
    
output = wave.open(outfile, 'wb')
output.setparams(data[0][0])
for i in range(len(data)):
    output.writeframes(data[i][1])
output.close()